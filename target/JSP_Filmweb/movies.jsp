<%@ page import="com.sda.jsp.model.Movie" %>
<%@ page import="java.util.List" %>
<%@ page import="com.sda.jsp.model.Person" %>
<%--
  Created by IntelliJ IDEA.
  User: wasyl
  Date: 11.03.2018
  Time: 11:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Movies</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css"/>
</head>
<body>
<nav class="site-header sticky-top py-1">
    <div class="container d-flex flex-column flex-md-row justify-content-between">
        <a class="py-2" href="#">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                 stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                 class="d-block mx-auto">
                <circle cx="12" cy="12" r="10"></circle>
                <line x1="14.31" y1="8" x2="20.05" y2="17.94"></line>
                <line x1="9.69" y1="8" x2="21.17" y2="8"></line>
                <line x1="7.38" y1="12" x2="13.12" y2="2.06"></line>
                <line x1="9.69" y1="16" x2="3.95" y2="6.06"></line>
                <line x1="14.31" y1="16" x2="2.83" y2="16"></line>
                <line x1="16.62" y1="12" x2="10.88" y2="21.94"></line>
            </svg>
        </a>
        <a class="py-2 d-none d-md-inline-block" href="#">Movies</a>
        <a class="py-2 d-none d-md-inline-block" href="#">People</a>
        <a class="py-2 d-none d-md-inline-block" href="#">Features</a>
    </div>
</nav>
<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 bg-light">
    <div class="col-md-10 p-lg-10 mx-auto my-10">
        <h1 class="font-weight-normal">Movies</h1>

        <div class="row col-lg-12">
            <div class="col-md-1"><h4>Id</h4></div>
            <div class="col-md-3"><h4>Name</h4></div>
            <div class="col-md-1"><h4>Rate</h4></div>
            <div class="col-md-2"><h4>Director</h4></div>
            <div class="col-md-3"><h4>Release Date</h4></div>
        </div>
        <%
            List<Movie> movies = (List<Movie>) request.getAttribute("movies");
            StringBuilder builder = new StringBuilder();

            int i = 0;
            for (Movie m : movies) {
                if (i % 2 == 0) {
                    builder.append("<div class=\"row white\">");
                } else {
                    builder.append("<div class=\"row black\">");
                }
                builder.append("<div class=\"col-md-1\">").append(m.getId()).append("</div>");
                builder.append("<div class=\"col-md-3\">").append(m.getName()).append("</div>");
                builder.append("<div class=\"col-md-1\">").append(m.getRate()).append("</div>");
                builder.append("<div class=\"col-md-2\">").append(m.getDirector()).append("</div>");
                builder.append("<div class=\"col-md-3\">").append(m.getReleaseDate()).append("</div>");
                builder.append("</div>");
                i++;
            }
            out.print(builder.toString());
        %>
        <form method="post" action="/movies">
            <div class="row">
                <div class="col-lg-3">
                    <span>Title</span>
                </div>
                <div class="col-lg-3">
                    <input class="form-control" type="text" name="movieName">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <span>Rate</span>
                </div>
                <div class="col-lg-3">
                    <input class="form-control" type="text" name="rate">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <span>Director</span>
                </div>

                <div class="col-lg-3">
                    <select class="form-control" name="director">
                            <%
                        List<Person> peoples = (List<Person>) request.getAttribute("directors");
                        for (Person p : peoples) {
                            out.println("<option value=\"" + p.getId() + "\" >" + p.getFirstName() + " " + p.getSurname() + "</option>");
                        }
                    %>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    Release Date
                </div>
                <div class="col-lg-3">
                    <input class="form-control" name="releaseDate" type="date"/>
                </div>
            </div>
            <input type="submit" value="Add Movie">
        </form>
    </div>
</div>

<footer class="container py-5">
    <div class="row">
        <div class="col-12 col-md">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                 stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                 class="d-block mb-2">
                <circle cx="12" cy="12" r="10"></circle>
                <line x1="14.31" y1="8" x2="20.05" y2="17.94"></line>
                <line x1="9.69" y1="8" x2="21.17" y2="8"></line>
                <line x1="7.38" y1="12" x2="13.12" y2="2.06"></line>
                <line x1="9.69" y1="16" x2="3.95" y2="6.06"></line>
                <line x1="14.31" y1="16" x2="2.83" y2="16"></line>
                <line x1="16.62" y1="12" x2="10.88" y2="21.94"></line>
            </svg>
            <small class="d-block mb-3 text-muted">&copy; 2017-2018</small>
        </div>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script>
    Holder.addTheme('thumb', {
        bg: '#55595c',
        fg: '#eceeef',
        text: 'Thumbnail'
    });
</script>
</body>


</html>
