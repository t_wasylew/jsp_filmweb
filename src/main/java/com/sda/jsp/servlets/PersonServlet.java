package com.sda.jsp.servlets;


import com.sda.jsp.dao.PersonDAO;
import com.sda.jsp.model.Person;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/person")
public class PersonServlet extends HttpServlet{

    private PersonDAO dao = new PersonDAO();
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<Person> personList = dao.getAllPerson();
            request.setAttribute("person", personList);
            request.getRequestDispatcher("/person.jsp").forward(request, response);
        } catch (IOException | ServletException e) {
            e.printStackTrace();
        }
    }
}
