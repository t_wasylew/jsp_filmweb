package com.sda.jsp.servlets;
import com.sda.jsp.dao.MoviesDAO;
import com.sda.jsp.dao.PersonDAO;
import com.sda.jsp.model.Movie;
import com.sda.jsp.model.Person;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@WebServlet(urlPatterns = "/movies")
public class MoviesServlet extends HttpServlet {
    private MoviesDAO dao = new MoviesDAO();
    private PersonDAO personDAO = new PersonDAO();
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<Movie> movieList = dao.getAllMovies();
            List<Person> personList = personDAO.getAllDirectors();
            request.setAttribute("movies", movieList);
            request.setAttribute("directors", personList);
            request.getRequestDispatcher("/movies.jsp").forward(request, response);

        } catch (IOException | ServletException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String movieName = request.getParameter("movieName");
        String director = request.getParameter("director");
        Double rate = Double.parseDouble(request.getParameter("rate"));
        String releaseDate = request.getParameter("releaseDate");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        if(movieName != null && releaseDate != null && rate != null) {
            Movie movie = new Movie();
            movie.setName(movieName);
            Person person1 = personDAO.getPersonByID(director);
            movie.setDirector(person1);
            movie.setRate(rate);
            movie.setReleaseDate(LocalDate.parse(releaseDate,formatter));
            dao.addMovie(movie);
        }

        response.sendRedirect("/movies");
    }
}
