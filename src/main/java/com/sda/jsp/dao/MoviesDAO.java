package com.sda.jsp.dao;

import com.sda.jsp.model.Movie;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;


import java.util.List;

public class MoviesDAO {


    public List<Movie> getAllMovies() {

        Session session = DatabaseSession.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Criteria criteria = session.createCriteria(Movie.class);
        List<Movie> movieList = criteria.list();
        transaction.commit();
        session.close();
        return movieList;
    }

    public void addMovie(Movie movie) {
        Session session = DatabaseSession.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(movie);
        transaction.commit();
        session.flush();
        session.close();
    }

}
