package com.sda.jsp.dao;

import com.sda.jsp.model.Movie;
import com.sda.jsp.model.Person;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class PersonDAO {

    public List<Person> getAllPerson() {

        Session session = DatabaseSession.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Criteria criteria = session.createCriteria(Person.class);
        List<Person> personList = criteria.list();
        transaction.commit();
        session.close();
        return personList;
    }

    public List<Person> getAllDirectors() {
        Session session = DatabaseSession.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("FROM person WHERE profession='DIRECTOR'");
        List<Person> directorList = (List<Person>)query.list();
        transaction.commit();
        session.flush();
        return directorList;
    }


    public Person getPersonByID(String director) {
        int id = Integer.parseInt(director);
        Session session = DatabaseSession.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("FROM person WHERE id='" + id + "'");
        Person person = (Person) query.uniqueResult();
        transaction.commit();
        session.flush();
        return person;
    }
}
